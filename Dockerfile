FROM node:lts-alpine AS builder

WORKDIR /app

RUN apk update -qq && apk add python-dev openssh git g++ make curl linux-headers bash openssl

COPY package.json package-lock.json ./
RUN npm config set unsafe-perm true
RUN npm install -g nodemon@2.0.7 ts-node@9.1.1
RUN npm install
RUN npm config set unsafe-perm false

FROM node:lts-alpine

WORKDIR /app

RUN apk add bash openssl ca-certificates graphicsmagick ghostscript

COPY --from=builder /app/node_modules /node_modules
COPY --from=builder /usr/local/bin /usr/local/bin
COPY --from=builder /usr/local/lib /usr/local/lib
