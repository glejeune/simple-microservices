import { expect } from 'chai';
import { agent as request } from 'supertest';
import 'mocha';

import App from '../src/index';

describe('status controller', () => {
  it('should GET', async () => {
    const res = await request(App).get('/v1/status');
    expect(res.status).to.equal(200);
    expect(res.type).to.equal('application/json');
    expect(res.body.message).to.equal('OK');
  });
});
