# Microservices with Node, RabbitMQ and MongoDB

## Development environment

### Start the devenv

1. Install [docker](https://www.docker.com/) and [direnv](https://direnv.net/)(optional).
2. Clone this repo: `git clone https://gitlab.com/glejeune/simple-microservices.git`.
3. If you have installed _direnv_, in the repo, run `direnv allow`
4. In the repo, run `npm install`
5. Generate the TSOA spec and routes by running `npm run tsoa:gen`
6. Build the devenv :

   - With _direnv_: `sms-devenv build`
   - Without _direnv_: `./devenv/sms-devenv build`

7. Start the devenv :

   - With _direnv_: `sms-devenv start`
   - Without _direnv_: `./devenv/sms-devenv start`

8. Wait for all containers to be started, then go to http://localhost:3001/docs/

### Stop the devenv

In the repo:

- With _direnv_: `sms-devenv stop`
- Without _direnv_: `./devenv/sms-devenv start`

### `sms-devenv` commands

`up` or `start`: start all devenv containers.

`down` or `stop`: stop all devenv containers.

`kill` or `clean`: stop and clean the devenv.

`restart`: restart all devenv containers.

`exec <container> [command]`: execute a commande in a container. If no command is given, open a shell in the container.

`ps`: List containers.

`build`: Build devenv containers.

`pull`: Pull devenv containers.

`logs [-f] [--tail=<number>] [container [...]]`: Show one or more containers los.

### Useful URLs:

- API Documentation (swagger): http://localhost:3001/docs/
- RabbitMQ manager (login: guest, password: guest): http://localhost:15672

## Todo

- [ ] Add tests... a LOT of tests !!!
- [ ] Create a rollup config to build all services
- [ ] Add a PM2 config to run services with PM2

## License

BSD 3-Clause License

Copyright (c) 2021, Gregoire Lejeune<br />
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

- Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

- Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

- Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
