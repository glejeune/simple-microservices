import * as path from 'path';
import * as dotenv from 'dotenv';

const envFile = path.resolve(process.cwd(), process.env.ENV_FILE || '.env');

dotenv.config({
  path: envFile,
});

export default process.env;
