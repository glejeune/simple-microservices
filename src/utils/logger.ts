import { createLogger, transports, format, Logger } from 'winston';
import config from './config';

const logger: Logger = createLogger({
  level: config.LOG_LEVEL || 'info',
  format: format.json(),
  exitOnError: false,
});

interface IFormat {
  level: string;
  message: string;
  timestamp: string;
}

const myFormat = format.printf(({ level, message, timestamp }: IFormat): string => {
  return `[${timestamp}] [${level}] ${message}`;
});

if (config.NODE_ENV !== 'production') {
  logger.add(
    new transports.Console({
      level: config.LOG_LEVEL || 'info',
      format: format.combine(format.timestamp(), myFormat),
    })
  );
}

export default logger;
