import * as bodyParser from 'body-parser';
import * as express from 'express';
import * as swaggerUi from 'swagger-ui-express';

import { RegisterRoutes } from './router/routes';
import logger from './utils/logger';
import config from './utils/config';
import loggerMiddelware from './middleware/logger';

class App {
  express: express.Application;

  constructor() {
    this.express = express();
    this.middleware();
    this.routes();
    this.statics();
    this.setErrorHandler();
    this.startSwagger();
  }

  private middleware(): void {
    this.express.use(bodyParser.json());
    this.express.use(bodyParser.urlencoded({ extended: false }));
    this.express.use(loggerMiddelware);
  }

  private routes(): void {
    RegisterRoutes(this.express);
  }

  private setErrorHandler(): void {
    this.express.use(
      (err: any, _req: express.Request, res: express.Response, next: express.NextFunction): express.Response | void => {
        if (err && 'status' in err) {
          // eslint-disable-next-line
          return res.status(err.status).json({
            // eslint-disable-next-line
            message: `${err.message}`,
          });
        }
        if (err instanceof Error) {
          return res.status(500).json({
            message: 'Internal Server Error',
          });
        }

        next();
      }
    );
  }

  private statics(): void {
    this.express.use('/static', express.static(config.STORE_DIR));
  }

  private startSwagger(): void {
    try {
      this.express.use('/docs', swaggerUi.serve, async (_req: express.Request, res: express.Response) => {
        return res.send(swaggerUi.generateHTML(await import('./swagger.json')));
      });
    } catch (e) {
      logger.error(e);
    }
  }
}
export default new App().express;
