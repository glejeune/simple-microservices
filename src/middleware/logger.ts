import { Request, Response } from 'express';

import logger from '../utils/logger';

export default (req: Request, _res: Response, next: () => any): any => {
  logger.info(`[${req.method}] ${req.originalUrl} - ${req.ip}${req.ips.length > 0 ? ' - ' + req.ips.join('|') : ''}`);
  next();
};
