import * as amqp from 'amqplib/callback_api';

import type { Message } from 'amqplib';
import type { Connection, Channel } from 'amqplib/callback_api';

import logger from '../utils/logger';
import config from '../utils/config';
import { IDocumentSchema } from '../model/document';

export type IMessage = IDocumentSchema;

export const QUEUE_PDF_THUMBNAIL = 'QUEUE_PDF_THUMBNAIL';

export interface IConsumer {
  readonly process: (content: IMessage, msg?: Message | null) => Promise<any>;
  readonly afterAck?: (data: any, msg?: Message | null) => Promise<void>;
  readonly afterNoAck?: (err: Error, msg?: Message | null) => Promise<void>;
  readonly afterAll?: (err?: Error | null, data?: any | null, msg?: Message | null) => Promise<void>;
}

export interface IOptions {
  retryOnError: boolean;
}

export class AMQP {
  conn: Connection | undefined | null = null;
  channel: Channel | undefined | null = null;
  restartTimer: NodeJS.Timeout = null;

  public async connect(): Promise<void> {
    const conn: Connection = await new Promise<Connection>((resolve, reject) =>
      amqp.connect(config.AMQP_URL, (err, con) => (err ? reject(err) : resolve(con)))
    );

    conn.on('error', (error: Error) => {
      logger.error(`Brocker error: ${error.message}`);
      if (error.message !== 'Connection closing') {
        this.doRestart(error);
      }
    });

    this.conn = conn;
    this.channel = await new Promise<Channel>((resolve, reject) =>
      conn.createChannel((err, c) => (err ? reject(err) : resolve(c)))
    );
  }

  public send(message: IMessage, queueName: string): void {
    logger.debug(`Queue ${JSON.stringify(message)} to ${queueName}`);
    this.channel.assertQueue(queueName, { durable: true });
    // eslint-disable-next-line
    // @ts-ignore
    this.channel.sendToQueue(queueName, Buffer.from(JSON.stringify(message)), { durable: true });
  }

  public addConsumer(consumer: IConsumer, queueName: string): void {
    this.channel.assertQueue(queueName, { durable: true });
    this.channel.prefetch(1);
    this.channel.consume(
      queueName,
      (data?: Message | null) => {
        const { content = Buffer.from('{}') } = data || {};
        const message: IMessage = JSON.parse(content.toString()) as IMessage;

        consumer
          .process(message, data)
          .then((consumeResult: any) => {
            if (data) {
              this.channel.ack(data);
              if (consumer.afterAck) {
                void consumer.afterAck(consumeResult, data).then(() => {
                  if (consumer.afterAll) {
                    void consumer.afterAll(null, consumeResult, data);
                  }
                });
              }
            }
          })
          .catch((consumeError: Error) => {
            this.channel.nack(data);
            if (consumer.afterNoAck) {
              void consumer.afterNoAck(consumeError, data).then(() => {
                if (consumer.afterAll) {
                  void consumer.afterAll(consumeError, null, data);
                }
              });
            }
          });
      },
      { noAck: false }
    );
  }

  doRestart(error?: Error): void {
    if (error) {
      logger.error(`Message broker got error: ${error.message}`);
    }
    if (this.restartTimer) {
      clearTimeout(this.restartTimer);
    }
    this.restartTimer = setTimeout(() => void this.connect(), 5000);
  }
}
