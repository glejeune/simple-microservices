import { Controller, Route, Get, Tags, SuccessResponse } from 'tsoa';

interface IStatusResponse {
  message: string;
}

@Route('status')
@Tags('StatusController')
export class StatusController extends Controller {
  /**
   * Check server status
   */
  @SuccessResponse(200, 'OK')
  @Get()
  status(): IStatusResponse {
    return { message: 'OK' };
  }
}
