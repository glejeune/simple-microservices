import { Controller, Route, Post, Get, Tags, Body, SuccessResponse, Response } from 'tsoa';

import { IDocumentCreate, IDocumentSchema, Document } from '../model/document';

@Route('document')
@Tags('DocumentController')
export class DocumentController extends Controller {
  /**
   * Retrieve documents
   */
  @Get('all')
  async all(): Promise<IDocumentSchema[]> {
    return await Document.find({}).then((docs: Document[]) => docs.map((doc: Document) => doc.asMessage()));
  }

  /**
   * Create a new document
   */
  @Response(422, 'Validation Failed')
  @SuccessResponse('201', 'Created')
  @Post('create')
  create(@Body() body: IDocumentCreate): void {
    this.setStatus(201);
    const document: Document = new Document(body);
    void document.save();
    return;
  }
}
