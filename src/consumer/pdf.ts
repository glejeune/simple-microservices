import * as fs from 'fs/promises';
import * as gm from 'gm';
import * as path from 'path';
import { MongoClient } from 'mongodb';
import axios, { AxiosResponse } from 'axios';

import { IConsumer } from '../service/amqp';
import { IDocumentSchema, Document } from '../model/document';
import logger from '../utils/logger';
import config from '../utils/config';

export class PDFConsumer implements IConsumer {
  mongoClient: MongoClient;

  constructor(client: MongoClient) {
    this.mongoClient = client;
  }

  async process(message: IDocumentSchema): Promise<any> {
    logger.debug(`Process document ${JSON.stringify(message)}`);
    return axios
      .get<Buffer>(message.url)
      .then((response: AxiosResponse<Buffer>) => {
        logger.debug(`Save document ${message._id}.pdf to store`);
        return fs.writeFile(path.join(config.STORE_DIR, `${message._id}.pdf`), response.data);
      })
      .then(() => {
        logger.debug(`Generate PNG thumbnail for ${message._id}.pdf`);
        gm(path.join(config.STORE_DIR, `${message._id}.pdf[0]`))
          .setFormat('png')
          .resize(
            config.PDF_THUMB_WIDTH ? parseInt(config.PDF_THUMB_WIDTH, 10) : 200,
            config.PDF_THUMB_HEIGHT ? parseInt(config.PDF_THUMB_HEIGHT, 10) : 200
          )
          .quality(config.PDF_THUMB_QUALITY ? parseInt(config.PDF_THUMB_QUALITY, 10) : 80)
          .write(path.join(config.STORE_DIR, `${message._id}.png`), (error) => {
            if (!error) {
              logger.debug(`Thumbnail ${message._id}.png created!`);
              void Document.first({ _id: message._id }).then((doc: Document | undefined) => {
                if (doc) {
                  void doc.update({
                    thumbnailUrl: `${config.STATIC_PATH}/${message._id}.png`,
                    localUrl: `${config.STATIC_PATH}/${message._id}.pdf`,
                  });
                }
              });
            } else {
              logger.error(`Failed to create thumbnail for ${message._id}.pdf: ${error.message}`);
            }
          });
      })
      .catch((error: Error) => {
        logger.error(`Failed to download ${message.url}: ${error.message}`);
      });
  }
}
