import { v4 as uuidv4 } from 'uuid';
import { FilterQuery, Collection, Db } from 'mongodb';

import { MongoHelper } from '../service/mongo_helper';
import config from '../utils/config';
import logger from '../utils/logger';

export interface IDocumentCreate {
  url: string;
  title?: string;
}

export interface IDocumentSchema {
  _id: string;
  title?: string;
  localUrl?: string;
  url: string;
  thumbnailUrl?: string;
}

export class Document {
  private _db: Db;
  private _collection: Collection<IDocumentSchema>;
  public _id: string;
  public url: string;
  public localUrl: string;
  public title: string;
  public thumbnailUrl: string;

  constructor(document?: IDocumentCreate) {
    this._db = MongoHelper.client.db(config.MONGODB_BASE);
    this._collection = this._db.collection('documents');
    if (document) {
      this.url = document.url;
      this.title = document.title;
    }
  }

  public asMessage(): IDocumentSchema {
    return {
      _id: this._id,
      url: this.url,
      localUrl: this.localUrl,
      title: this.title,
      thumbnailUrl: this.thumbnailUrl,
    };
  }

  private static fromDocumentSchema(documentSchema: IDocumentSchema): Document {
    const document = new Document();
    document.url = documentSchema.url;
    document.localUrl = documentSchema.localUrl;
    document.title = documentSchema.title;
    document.thumbnailUrl = documentSchema.thumbnailUrl;
    document._id = documentSchema._id;
    return document;
  }

  public static async find(query: FilterQuery<IDocumentSchema>): Promise<Document[]> {
    const db = MongoHelper.client.db(config.MONGODB_BASE);
    const collection = db.collection('documents');
    return collection
      .find(query)
      .toArray()
      .then((docs: IDocumentSchema[]) => {
        if (docs) {
          return docs.map((doc: IDocumentSchema) => this.fromDocumentSchema(doc));
        }
        return [];
      })
      .catch((err: Error) => {
        logger.error(`Retrieve documents failed: ${err.message}`);
        return [];
      });
  }

  public static async first(query: FilterQuery<IDocumentSchema>): Promise<undefined | Document> {
    const db = MongoHelper.client.db(config.MONGODB_BASE);
    const collection = db.collection('documents');
    return collection
      .find(query)
      .limit(1)
      .toArray()
      .then((docs: IDocumentSchema[]) => {
        if (docs) {
          return this.fromDocumentSchema(docs[0]);
        }
        return undefined;
      });
  }

  public async update(schema: Record<string, unknown>): Promise<void> {
    await this._collection.updateOne({ _id: this._id }, { $set: schema }).then(() => {
      ['url', 'title', 'thumbnailUrl'].forEach((key: string) => {
        if (key in schema) {
          // this[key] = schema[key];
        }
      });
    });
  }

  public save(): void {
    if (this._id) {
      logger.error("Can't save a document with an _id");
    } else {
      this._id = uuidv4();
      void this._collection
        .insertOne(this.asMessage())
        .then(() => {
          logger.info(`Document ${this._id} created.`);
        })
        .catch((err: Error) => {
          logger.error(`Save document failed: ${err.message}`);
        });
    }
  }
}
