import { MongoClient } from 'mongodb';

import { MongoHelper } from '../service/mongo_helper';
import { AMQP, QUEUE_PDF_THUMBNAIL } from '../service/amqp';
import { PDFConsumer } from '../consumer/pdf';
import config from '../utils/config';
import logger from '../utils/logger';

logger.debug(`Try to connect to ${config.MONGODB_URL}`);
void MongoHelper.connect(config.MONGODB_URL)
  .then((client: MongoClient) => {
    const amqp = new AMQP();
    return new Promise((resolve, reject) => {
      void amqp
        .connect()
        .then(() => resolve({ client, amqp }))
        .catch((error: Error) => reject(error));
    });
  })
  .then(({ client, amqp }: { client: MongoClient; amqp: AMQP }) => {
    amqp.addConsumer(new PDFConsumer(client), QUEUE_PDF_THUMBNAIL);
  })
  .catch((error: Error) => {
    logger.error(`Brocker error: ${error.message}`);
  });
