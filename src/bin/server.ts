import * as http from 'http';

import { MongoHelper } from '../service/mongo_helper';
import config from '../utils/config';
import logger from '../utils/logger';
import App from '..';

const server = http.createServer(App);

const normalizePort = (val: number | string): number | string | undefined => {
  const normolizedPort = typeof val === 'string' ? parseInt(val, 10) : val;
  if (isNaN(normolizedPort)) {
    return val;
  }

  if (normolizedPort >= 0) {
    return normolizedPort;
  }

  return undefined;
};

const port = normalizePort(config.PORT || 3000);
App.set('port', port || false);

const onError = (error: NodeJS.ErrnoException) => {
  if (error.syscall !== 'listen') {
    throw error;
  }
  const bind = typeof port === 'string' ? `Pipe ${port}` : `Port ${port}`;
  switch (error.code) {
    case 'EACCES':
      logger.error(`${bind} requires elevated privileges`);
      process.exit(1);
      break;
    case 'EADDRINUSE':
      logger.error(`${bind} is already in use`);
      process.exit(1);
      break;
    default:
      throw error;
  }
};

const onListening = () => {
  const addr = server.address();
  const bind = typeof addr === 'string' ? `pipe ${addr}` : `port ${addr ? addr.port : ''}`;
  logger.info(`Listening on ${bind}`);
};

logger.info(config.MONGODB_URL);

void MongoHelper.connect(config.MONGODB_URL).then(() => {
  // This is UGLY but sometimes... you know...
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  server.listen(port, config.IP_BIND);
  server.on('error', onError);
  server.on('listening', onListening);
});
