import { MongoClient, ChangeEvent } from 'mongodb';

import { MongoHelper } from '../service/mongo_helper';
import { IDocumentSchema, Document } from '../model/document';
import { AMQP, QUEUE_PDF_THUMBNAIL } from '../service/amqp';
import config from '../utils/config';
import logger from '../utils/logger';

logger.debug(`Try to connect to ${config.MONGODB_URL}`);
void MongoHelper.connect(config.MONGODB_URL)
  .then((client: MongoClient) => {
    const amqp = new AMQP();
    return new Promise((resolve, reject) => {
      void amqp
        .connect()
        .then(() => resolve({ client, amqp }))
        .catch((error: Error) => reject(error));
    });
  })
  .then(({ client, amqp }: { client: MongoClient; amqp: AMQP }) => {
    const database = client.db(config.MONGODB_BASE);
    const documents = database.collection('documents');

    void Document.find({ thumbnailUrl: { $exists: false } }).then((docs: Document[]) => {
      docs.forEach((doc: Document) => {
        amqp.send(doc, QUEUE_PDF_THUMBNAIL);
      });
    });

    const changeStream = documents.watch();
    changeStream.on('change', (event: ChangeEvent<IDocumentSchema>) => {
      if (event.operationType === 'insert') {
        amqp.send(event.fullDocument, QUEUE_PDF_THUMBNAIL);
      }
    });
  })
  .catch((error: Error) => {
    logger.error(`Brocker error: ${error.message}`);
  });
