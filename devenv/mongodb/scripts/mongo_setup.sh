#!/bin/bash
sleep 10

echo "SETUP time now: `date +"%T"` - rs: ${MONGODB_REPLICASET_NAME}"
mongo --host sms-mongodb1:27017 <<EOF
  var cfg = {
    "_id": "${MONGODB_REPLICASET_NAME}",
    "version": 1,
    "members": [
      {
        "_id": 0,
        "host": "sms-mongodb1:27017",
        "priority": 2
      },
      {
        "_id": 1,
        "host": "sms-mongodb2:27017",
        "priority": 0
      },
      {
        "_id": 2,
        "host": "sms-mongodb3:27017",
        "priority": 0
      }
    ]
  };
  rs.initiate(cfg, { force: true });
  rs.reconfig(cfg, { force: true });
  db.getMongo().setReadPref('nearest');
EOF
